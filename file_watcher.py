import os
from os import path
import time

from watchdog.observers import Observer
from watchdog.events import (
    FileSystemEventHandler, FileCreatedEvent, PatternMatchingEventHandler)

import config
import db
from db_models import Base, File


class WatchdogWatcher:
    """Watches a directory for changes using the Watchdog library."""
    def __init__(self, event_handler: FileSystemEventHandler):
        """
        :param event_handler: the event handler instance that will be used.
        """
        self._observer = Observer()
        # We're prefixing the below with an underscore even though it might be
        # tempting to allow users of the class to alter it after construction
        # so we don't have to worry about what to do if it happens after we've
        # already started watching the directory.
        self._event_handler = event_handler

    def start_watching(self):
        """Start watching the configured directory for changes."""
        self._observer.schedule(self._event_handler, config.WATCHED_DIRECTORY)
        self._observer.start()

    def stop_watching(self):
        """Stop watching the configured directory for changes."""
        self._observer.stop()
        self._observer.join()


class FileRepositoryEventHandler(PatternMatchingEventHandler):
    """
    A PatternMatchingEventHandler which takes care of creating and updating
    file repository records as it is notified about file system events.
    """
    def on_any_event(self, event):
        print(f'Got FS event: {repr(event)}')

    def on_created(self, event):
        with db.session_scope() as session:
            file_name = File.name_from_path(event.src_path)
            file_record = session.query(File)\
                .filter(File.name == file_name).one_or_none()

            if file_record is None:
                file_record = File()
                session.add(file_record)

            file_record.update_from_path(event.src_path)

    def on_deleted(self, event):
        with db.session_scope() as session:
            file_name = File.name_from_path(event.src_path)
            file_record = session.query(File)\
                .filter(File.name == file_name).one_or_none()

            if file_record is None:
                # Somehow we didn't know about this file. Eventually we may
                # want to log this in a more structured way.
                print(f'Warning: file {file_name} was deleted, but no file '
                      f'record was found in the database for it.')
                return

            session.delete(file_record)

    def on_moved(self, event):
        self._on_change(event.src_path, event.dest_path)

    def on_modified(self, event):
        self._on_change(event.src_path, event.src_path)

    def _on_change(self, old_path, new_path):
        # The logic for file moves and file modification is very similar,
        # so we can share it here -- a file modification just becomes a move
        # with the old and new paths the same.
        with db.session_scope() as session:
            old_name = File.name_from_path(old_path)

            file_record = session.query(File)\
                .filter(File.name == old_name).one_or_none()

            if file_record is None:
                # We didn't yet know about this file, so we'll create it now:
                synthetic_created_event = FileCreatedEvent(new_path)
                return self.dispatch(synthetic_created_event)

            file_record.update_from_path(new_path)


def get_existing_files(event_handler):
    """List the extant files in the directory."""
    with os.scandir(config.WATCHED_DIRECTORY) as dir_iterator:
        for dir_entry in dir_iterator:
            if dir_entry.is_file():
                synthetic_created_event = FileCreatedEvent(dir_entry.path)
                event_handler.dispatch(synthetic_created_event)


if __name__ == '__main__':
    print(f'Creating tables, if necessary...')
    Base.metadata.create_all(db.engine)

    print(f'Ensuring {config.WATCHED_DIRECTORY} exists...')
    os.makedirs(config.WATCHED_DIRECTORY, exist_ok=True)

    event_handler = FileRepositoryEventHandler(
        ignore_directories=True, case_sensitive=config.CASE_SENSITIVE)

    print(f'Adding existing files in {config.WATCHED_DIRECTORY}...')
    get_existing_files(event_handler)

    print(f'Watching for changes in {config.WATCHED_DIRECTORY} '
          f'(case {"sensitive" if config.CASE_SENSITIVE else "insensitive"})')
    watcher = WatchdogWatcher(event_handler)

    try:
        watcher.start_watching()
        # The watcher will watch for file changes in a different thread, so we
        # can just sleep until somebody stops us.
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        print('Stopping file watcher...')
        watcher.stop_watching()
