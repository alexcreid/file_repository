from datetime import datetime, timezone
import os
from os import path
from typing import Any

from sqlalchemy import Column, BigInteger, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base


#  We need to override the inferred type of Base here due to a bug in mypy
# (https://github.com/python/mypy/issues/2477)
Base: Any = declarative_base()


class File(Base):
    """Records data about a file in the repository."""
    __tablename__ = 'files'

    id = Column(Integer, primary_key=True)
    # Most common file systems limit file names to 255 characters (or 255
    # bytes) so this should be long enough in practice.
    name = Column(String(255), index=True, unique=True)
    # We can't easily get our hands on a file's creation time on Linux (and
    # it's not guaranteed to be supported at all on all file systems), so we'll
    # instead track the time at which we first became aware of the file.
    added_at = Column(DateTime, index=True, default=datetime.utcnow)
    modification_time = Column(DateTime)
    # A BigInteger column should make us good for files up to (but not
    # including) 8 EiB. Some filesystems permit files larger than that, so
    # the method for storing the size would have to be revisited if *extremely*
    # large files are to be stored.
    size = Column(BigInteger)

    def update_from_path(self, pth: str):
        """
        Given a path, update the File record's name, modification time, and
        size from the attributes of the given file.
        """
        stat_result = os.stat(pth)
        self.name = self.name_from_path(pth)
        self.size = stat_result.st_size
        self.modification_time = datetime.fromtimestamp(
            stat_result.st_mtime, tz=timezone.utc)

    @classmethod
    def name_from_path(cls, pth: str) -> str:
        return path.basename(pth)
