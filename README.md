# File Repository
hello@alexreid.org

This is the inital work for a simple file repository which monitors a directory for added files, and makes a records of those files available via a simple JSON API.

## Installation
Prerequisites:
- Python 3 (tested with 3.7, Python 3.6 will also most likely work)
- A running MySQL server (the default port on localhost is the expected location, but this can be altered by changing the DB_URL value in config.py. It also expects to be able to log in as root with no password, but the user and password can also be edited in the same URL).

It's recommended that all these commands are run from within a virtual environment created for the purpose, to avoid affecting your system Python install!

From the root of the repository (the same directory as this readme), install the requirements:

```
pip install -r requirements.txt
```

Create the database:

```
mysql -u root -e "CREATE DATABASE file_repository"
```

## Running

There are two processes that need to be run, the file watcher and the API server. Again from the root of the repository (and with the virtual environment activated, if applicable):

```
python file_watcher.py
```

This will automatically create the needed database table and a folder to put files in (`put_files_here/` inside the repository root), and start watching for file changes. If the command is stopped, upon being restarted the current state of the files directory will be ascertained and the database updated.

The API server can be run as follows:

```
FLASK_APP=api flask run
```

The API can then be accessed by making HTTP requests to http://localhost:5000/available_files (to list the currently available files) and http://localhost:5000/archived_files (to list archived files, those that were added more than 5 days ago). For example:

```
--> GET http://localhost:5000/available_files
<-- 200 OK:
[
    {
        "added_at":"2019-02-06T22:54:05+00:00",
        "modification_time":"2019-02-06T20:44:04+00:00",
        "name":"my_file",
        "size":1275
    },
    ...
]
```

## Limitations

Currently, we're using the Flask dev server. In production, we'd want to use an alternative WSGI server, such as Gunicorn.

We'd also want to make an upstart service (or similar) to ensure that the file watcher continues to run at all times.

It would be fairly easy to containerise these components, along with the MySQL server, so that the entire thing can be easily run using Docker, for example.
