from datetime import datetime

from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
from marshmallow import fields, Schema

from . import config
from .db_models import Base, File

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = config.DB_URL
db = SQLAlchemy(app, model_class=Base)


@app.route('/available_files', methods=['GET'])
def list_available_files():
    filter_condition = File.added_at >= (datetime.utcnow() -
                                         config.ARCHIVE_AFTER)

    return _list_files(filter_condition)


@app.route('/archived_files', methods=['GET'])
def list_archived_files():
    filter_condition = File.added_at < (datetime.utcnow() -
                                        config.ARCHIVE_AFTER)

    return _list_files(filter_condition)


def _list_files(filter_condition):
    files = db.session.query(File)\
        .filter(filter_condition)\
        .order_by(File.added_at.desc())

    schema = FileSchema()

    output = schema.dump(files, many=True)
    return jsonify(output)


class FileSchema(Schema):

    """The schema used for rendering file records for the API."""

    name = fields.Str()
    added_at = fields.DateTime()
    modification_time = fields.DateTime()
    size = fields.Int()
