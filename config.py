"""Configuration options for the entire file repository."""

from datetime import timedelta

WATCHED_DIRECTORY = './put_files_here/'
CASE_SENSITIVE = True

DB_URL = 'mysql+mysqldb://root:@127.0.0.1/file_repository'
ARCHIVE_AFTER = timedelta(days=5)
